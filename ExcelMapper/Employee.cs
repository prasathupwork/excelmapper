﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExcelMapper
{
    public class Employee
    {
        public string EmpNumber { get; set; }
        public string EmpName { get; set; }
        public string ProjNumber { get; set; }
        public DateTime WeekEndingDate { get; set; }
        public DateTime ActualPayrollDate { get; set; }
        public string PayType { get; set; }
        public decimal Hours { get; set; }
        public decimal Amount { get; set; }
        public decimal HourlyRate { get; set; }
        public string AgencyName { get; set; }
        public string FirstName
        {
            get
            {
                EmpName = EmpName ?? string.Empty;
                if (EmpName.IndexOf(',') > 0)
                {
                    return EmpName.Substring(0, EmpName.IndexOf(','));
                }
                else
                {
                    return EmpName;
                }
            }
        }

        public string EmpNumberInt
        {
            get
            {
                EmpNumber = EmpNumber ?? string.Empty;
                return EmpNumber.TrimStart(' ', '0');
            }
        }
    }
}
