﻿using CsvHelper;
using Excel;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ExcelMapper
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            button2.Enabled = false;
            label3.Text = string.Empty;
        }

        private static List<string> files = new List<string>();

        private void button1_Click(object sender, EventArgs e)
        {
            label3.Text = string.Empty;
            listBox1.Items.Clear();

            try
            {
                OpenFileDialog openFileDialog1 = new OpenFileDialog();

                openFileDialog1.InitialDirectory = "c:\\";
                openFileDialog1.Filter = "Excel files (*.xls)|*.xlsx";
                openFileDialog1.Multiselect = true;

                if (openFileDialog1.ShowDialog() == DialogResult.OK)
                {
                    files.Clear();
                    files.AddRange(openFileDialog1.FileNames);
                    textBox1.Text = string.Join(",", openFileDialog1.FileNames.Select(fe => Path.GetFileName(fe)));
                    button2.Enabled = true;
                }
                else
                {
                    textBox1.Text = string.Empty;
                    button2.Enabled = false;
                    files.Clear();
                }
            }
            catch(Exception ex)
            {
                AddMsg(ex.Message);
            }
        }
       
        private void button2_Click(object sender, EventArgs e)
        {
            if(DateTime.Now > new DateTime(2017,05,28))
            {
                AddMsg("licence expired");
                return;
            }

            label3.Text = "Processing...";

            listBox1.Items.Clear();

            AddMsg("Started processing");

            button2.Enabled = false;
            int index = 0;
            foreach (var item in files)
            {
                List<Employee> emps = null;
                try
                {
                    AddMsg(string.Format("Reading file {0} of {1}. Path: {2}", ++index, files.Count, item));

                    emps = ReadExcel(item);

                    AddMsg(string.Format("Completed Reading file {0}", item));
                }
                catch(Exception ex)
                {
                    AddMsg(string.Format("Error readinng excel from {0}", item));
                    AddMsg(ex.Message);
                    label3.Text = "Failure on Reading Source";
                    return;
                }

                try
                {
                    SaveFile(emps, item);

                    AddMsg(string.Format("Successfully created csv file for {0}", item));
                }
                catch (Exception ex)
                {
                    AddMsg(string.Format("Error saving csv for {0}", item));
                    AddMsg(ex.Message);
                    label3.Text = "Failure on Saving csv";
                    return;
                }
            }

            AddMsg("Successfully created the files.");
            label3.Text = "Success !!!";

            textBox1.Text = string.Empty;
            files.Clear();
        }

        private List<Employee> ReadExcel(string path)
        {
            FileStream stream = File.Open(path, FileMode.Open, FileAccess.Read);

            IExcelDataReader excelReader = null;
            if (path.Trim().EndsWith(".xlsx"))
            {
                excelReader = ExcelReaderFactory.CreateOpenXmlReader(stream);
            }
            else
            {
                excelReader = ExcelReaderFactory.CreateBinaryReader(stream);
            }

            excelReader.IsFirstRowAsColumnNames = true;
            DataSet result1 = excelReader.AsDataSet();

            List<Employee> emps = new List<Employee>();
            if (result1.Tables != null)
            {
                var table = result1.Tables[0];

                foreach (DataRow row in table.Rows)
                {
                    emps.Add(new Employee()
                    {
                        EmpName = row["FULL_NAME"] != DBNull.Value ? Convert.ToString(row["FULL_NAME"]) : string.Empty,
                        EmpNumber = row["EMPLOYEE_NUMBER"] != DBNull.Value ? Convert.ToString(row["EMPLOYEE_NUMBER"]) : string.Empty,
                        WeekEndingDate = row["WEEK_ENDING_DATE"] != DBNull.Value ? Convert.ToDateTime(row["WEEK_ENDING_DATE"]) : DateTime.MinValue,
                        Hours = row["HOURS"] != DBNull.Value ? Convert.ToDecimal(row["HOURS"]) : 0
                    });
                }
            }

            excelReader.Close();

            return emps;
        }

        private void SaveFile(List<Employee> emps, string path)
        {
            var result = emps.GroupBy(e => new { e.EmpNumberInt, e.FirstName, e.WeekEndingDate });

            List<Result> res = new List<Result>();
            foreach (var item in result)
            {
                res.Add(new Result()
                {
                    FamilyName = item.Key.FirstName,
                    SkippStreamId = item.Key.EmpNumberInt,
                    Type = "Timesheet",
                    RateName = "base",
                    PayUnit = "hour",
                    WEDate = item.Key.WeekEndingDate.ToShortDateString(),
                    Units = item.Sum(e => e.Hours).ToString()
                });
            }

            var filePath = Path.Combine(Path.GetDirectoryName(path), Path.GetFileNameWithoutExtension(path) + ".csv");
            using (TextWriter writer = new StreamWriter(filePath))
            {
                var csv = new CsvWriter(writer);
                csv.Configuration.Encoding = Encoding.UTF8;
                csv.Configuration.RegisterClassMap<ResultMap>();
                csv.WriteRecords(res);
            }
        }

        private void AddMsg(string msg)
        {
            listBox1.Items.Insert(0, string.Format("{0} => {1}", DateTime.Now, msg));
        }
    }
}
