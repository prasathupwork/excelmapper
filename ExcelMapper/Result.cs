﻿using CsvHelper.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExcelMapper
{
    public class Result
    {
        public string JobUIN { get; set; }
        public string RunRef { get; set; }
        public string RunDate { get; set; }
        public string Country { get; set; }
        public string Location { get; set; }
        public string HiringManager { get; set; }
        public string FirstName { get; set; }
        public string FamilyName { get; set; }
        public string TempType { get; set; }
        public string SkippStreamId { get; set; }

        public string BillingCostCentre { get; set; }
        public string Type { get; set; }
        public string ItemDetail { get; set; }
        public string RateName { get; set; }
        public string PayRate { get; set; }
        public string AgencyRate { get; set; }
        public string PayUnit { get; set; }
        public string TempStatus { get; set; }
        public string Month { get; set; }
        public string WEDate { get; set; }

        public string Units { get; set; }
        public string NumberOfDaysWorked { get; set; }
        public string Net { get; set; }
        public string InputGST { get; set; }
        public string AgencyName { get; set; }
        public string NonPanelSupplierName { get; set; }
    }

    public sealed class ResultMap : CsvClassMap<Result>
    {
        public ResultMap()
        {
            Map(m => m.JobUIN).Name("Job UIN");
            Map(m => m.RunRef).Name("Run Ref");
            Map(m => m.RunDate).Name("Run Date");
            Map(m => m.Country).Name("Country");
            Map(m => m.Location).Name("Location");
            Map(m => m.HiringManager).Name("Hiring Manager");
            Map(m => m.FirstName).Name("First Name");
            Map(m => m.FamilyName).Name("Family Name");
            Map(m => m.TempType).Name("Temp Type");
            Map(m => m.SkippStreamId).Name("Skillstream ID");

            Map(m => m.BillingCostCentre).Name("Billing Cost Centre");
            Map(m => m.Type).Name("Type");
            Map(m => m.ItemDetail).Name("Item Detail");
            Map(m => m.RateName).Name("Rate Name");
            Map(m => m.PayRate).Name("Pay Rate");
            Map(m => m.AgencyRate).Name("Agency Rate");
            Map(m => m.PayUnit).Name("Pay Unit");
            Map(m => m.TempStatus).Name("Temp Status");
            Map(m => m.Month).Name("Month");
            Map(m => m.WEDate).Name("W/E Date");

            Map(m => m.Units).Name("Units");
            Map(m => m.NumberOfDaysWorked).Name("Number of Days Worked");
            Map(m => m.Net).Name("Net");
            Map(m => m.InputGST).Name("Input GST");
            Map(m => m.AgencyName).Name("Agency Name");
            Map(m => m.NonPanelSupplierName).Name("Non-Panel Supplier Name");
        }
    }
}
